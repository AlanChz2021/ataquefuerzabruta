﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtaqueFuerzaBruta
{
	public partial class Form1 : Form
	{
		string NIP = "";
		public Form1()
		{
			InitializeComponent();
		}

		private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
				e.Handled = true;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			NIP = "";
			Reloj = 0;
			if (textBox1.Text.Length == 4)
			{
				timer1.Enabled = true;
				for (int x = 0; x < 4; x++)
				{
					for (int i = 0; i < 10; i++)
					{
						if (char.Parse(i.ToString()) == textBox1.Text[x])
						{
							NIP += i.ToString();
							timer1.Enabled = true;
						}
					}
				}
			}
		}

		int Reloj = 0;
		private void timer1_Tick(object sender, EventArgs e)
		{
			Random random = new Random();
			int randomNumber = random.Next(1000, 9999);

			if (Reloj >= 0 && Reloj <= 20)
			{
				label2.Text = randomNumber.ToString();
			}
			if (Reloj >= 21 && Reloj <= 40)
			{
				label2.Text = randomNumber.ToString().Substring(0, 3) + NIP.Substring(3, 1);
				timer1.Interval = 50;
			}
			if (Reloj >= 41 && Reloj <= 60)
			{
				label2.Text = randomNumber.ToString().Substring(0, 2) + NIP.Substring(2, 2);
				timer1.Interval = 90;
			}
			if (Reloj >= 61 && Reloj <= 70)
			{
				label2.Text = randomNumber.ToString().Substring(0, 1) + NIP.Substring(1, 3);
				timer1.Interval = 100;
			}

			if (Reloj == 71)
			{
				timer1.Enabled = false;
				label2.Text = NIP;
				MessageBox.Show("Bienvendio =)" + MessageBoxButtons.OK + MessageBoxIcon.Information);
			}

			Reloj++;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
